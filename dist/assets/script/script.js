'use strict';

(function () {
    var infoLeft = document.querySelector('.info-left');
    var infoLeftext1 = document.querySelector('.info-left').querySelector('h3');
    var infoLeftext2 = document.querySelector('.info-left').querySelector('p');
    var level1btn = document.querySelector('.constructor__level1').querySelector('.btn');
    var level2btn = document.querySelector('.constructor__level2').querySelectorAll('.constructor__level2__logo');
    var level3btn = document.querySelector('.constructor__level3').querySelectorAll('.constructor__level3__block');
    var level4btn = document.querySelector('.constructor__level4').querySelector('.btn__next');
    var level5btn = document.querySelector('.constructor__level5').querySelectorAll('.card__month');
    var level1 = document.querySelector('.constructor__level1');
    var level2 = document.querySelector('.constructor__level2');
    var level3 = document.querySelector('.constructor__level3');
    var level4 = document.querySelector('.constructor__level4');
    var level5 = document.querySelector('.constructor__level5');
    var level2btnBack = document.querySelector('.constructor__level2').querySelector('.btn');
    var level3btnBack = document.querySelector('.constructor__level3').querySelector('.btn');
    var level4btnBack = document.querySelector('.constructor__level4').querySelector('.btn__back');
    var level5btnBack = document.querySelector('.constructor__level5').querySelector('.btn__back');

    $(level1btn).on("click", function () {
        level1.style.display = "none";
        level2.style.display = "";
    });

    $(level2btnBack).on("click", function () {
        level1.style.display = "";
        level2.style.display = "none";
    });

    $(level2btn).on("click", function () {
        for (var i = 0; i < level2btn.length; i++) {
            if (this.className === level2btn[i].className) {
                infoLeft.removeChild(infoLeftext1);
                infoLeft.removeChild(infoLeftext2);
                var logo = level2btn[i].cloneNode(true);
                infoLeft.appendChild(logo);
            }
        }
        level2.style.display = "none";
        level3.style.display = "";
    });

    $(level3btnBack).on("click", function () {
        level2.style.display = "";
        level3.style.display = "none";
        infoLeft.appendChild(infoLeftext1);
        infoLeft.appendChild(infoLeftext2);
        infoLeft.removeChild(document.querySelector('.info-left').querySelector('.constructor__level2__logo'));
    });

    $(level3btn).on("click", function () {
        for (var i = 0; i < level3btn.length; i++) {
            if (this.className === level3btn[i].className) {
                var time = level3btn[i].cloneNode(true);
                infoLeft.appendChild(time);
            }
        }
        level3.style.display = "none";
        level4.style.display = "";
    });

    $(level4btnBack).on("click", function () {
        level3.style.display = "";
        level4.style.display = "none";
        infoLeft.removeChild(document.querySelector('.info-left').querySelector('.constructor__level3__block'));
    });

    $(level4btn).on("click", function () {
        var checked = document.querySelector('.constructor__level4__all').querySelectorAll('.checkbox');
        var inputChecked = document.querySelector('.constructor__level4__all').querySelectorAll('.checkbox__color__active');
        for (var i = 0; i < checked.length; i++) {
            if ($(checked[i]).prop('checked')) {
                var time = inputChecked[i].cloneNode(true);
                infoLeft.appendChild(time);
            }
        }
        level4.style.display = "none";
        level5.style.display = "";
    });

    $(level5btnBack).on("click", function () {
        level4.style.display = "";
        level5.style.display = "none";
        var inputChecked = document.querySelector('.info-left').querySelectorAll('.checkbox__color__active');
        for (var i = 0; i < inputChecked.length; i++) {
            infoLeft.removeChild(inputChecked[i]);
        }
    });

    $(level5btn).on("click", function () {
        document.querySelector('.constructor').style.display = "none";
        document.querySelector('.selected__card').style.display = "";
    });
})();
'use strict';

var btnRed = document.querySelector('.landing__red').querySelector('.btn__red__a');
var btnBlue = document.querySelector('.landing__blue').querySelector('.btn__blue__a');

var logoRed = document.querySelector('.landing__red').querySelector('.logo__red__a');
var logoBlue = document.querySelector('.landing__blue').querySelector('.logo__blue__a');

var body = document.querySelector('body');

btnBlue.addEventListener('click', handleUpdateBlue);
btnRed.addEventListener('click', handleUpdateRed);
logoBlue.addEventListener('click', handleUpdateBlue);
logoRed.addEventListener('click', handleUpdateRed);

var key = 'ruso_q_712899_example';

function handleUpdateRed() {
    window.localStorage[key] = "red";
    body.classList = "red";
}

function handleUpdateBlue() {
    window.localStorage[key] = "blue";
    body.classList = "blue";
}
'use strict';

var html = document.querySelector('html');
var textTitles = document.querySelectorAll('.why-choose__block__text');

window.addEventListener('scroll', function (e) {
    if (900 <= html.scrollTop) {
        //textTitles[0].classList = 'why-choose__block__text animation__text__left';
    }
});
"use strict";

// jquery.events.frame.js
// 1.1 - lite
// Stephen Band
//
// Project home:
// webdev.stephband.info/events/frame/
//
// Source:
// http://github.com/stephband/jquery.event.frame

(function (jQuery, undefined) {

    var timer;

    // Timer constructor
    // fn - callback to call on each frame, with context set to the timer object
    // fd - frame duration in milliseconds

    function Timer(fn, fd) {
        var self = this,
            clock;

        function update() {
            self.frameCount++;
            fn.call(self);
        }

        this.frameDuration = fd || 25;
        this.frameCount = -1;
        this.start = function () {
            update();
            clock = setInterval(update, this.frameDuration);
        };
        this.stop = function () {
            clearInterval(clock);
            clock = null;
        };
    }

    // callHandler() is the callback given to the timer object,
    // it makes the event object and calls the handler
    // context is the timer object

    function callHandler() {
        var fn = jQuery.event.special.frame.handler,
            event = jQuery.Event("frame"),
            array = this.array,
            l = array.length;

        // Give event object properties
        event.frameCount = this.frameCount;

        // Call handler on each elem in array
        while (l--) {
            fn.call(array[l], event);
        }
    }

    if (!jQuery.event.special.frame) {
        jQuery.event.special.frame = {
            // Fires the first time an event is bound per element
            setup: function setup(data, namespaces) {
                if (timer) {
                    timer.array.push(this);
                } else {
                    timer = new Timer(callHandler, data && data.frameDuration);
                    timer.array = [this];

                    // Queue timer to start as soon as this thread has finished
                    var t = setTimeout(function () {
                        timer.start();
                        clearTimeout(t);
                        t = null;
                    }, 0);
                }
                return;
            },
            // Fires last time event is unbound per element
            teardown: function teardown(namespaces) {
                var array = timer.array,
                    l = array.length;

                // Remove element from list
                while (l--) {
                    if (array[l] === this) {
                        array.splice(l, 1);
                        break;
                    }
                }

                // Stop and remove timer when no elems left
                if (array.length === 0) {
                    timer.stop();
                    timer = undefined;
                }
                return;
            },
            handler: function handler(event) {
                // let jQuery handle the calling of event handlers

                // In jQuery >= 1.9.0 jQuery.event.handle is remove, see:
                // http://jquery.com/upgrade-guide/1.9/#other-undocumented-properties-and-methods
                if (jQuery.event.handle) {
                    jQuery.event.handle.apply(this, arguments);
                } else {
                    jQuery.event.dispatch.apply(this, arguments);
                }
            }
        };
    }
})(jQuery);
"use strict";

if (!jQuery.event.special.frame) {

    // jquery.events.frame.js
    // 1.1 - lite
    // Stephen Band
    //
    // Project home:
    // webdev.stephband.info/events/frame/
    //
    // Source:
    // http://github.com/stephband/jquery.event.frame

    (function (d, h) {
        function i(a, b) {
            function e() {
                f.frameCount++;a.call(f);
            }var f = this,
                g;this.frameDuration = b || 25;this.frameCount = -1;this.start = function () {
                e();g = setInterval(e, this.frameDuration);
            };this.stop = function () {
                clearInterval(g);g = null;
            };
        }function j() {
            var a = d.event.special.frame.handler,
                b = d.Event("frame"),
                e = this.array,
                f = e.length;for (b.frameCount = this.frameCount; f--;) {
                a.call(e[f], b);
            }
        }var c;if (!d.event.special.frame) d.event.special.frame = { setup: function setup(a) {
                if (c) c.array.push(this);else {
                    c = new i(j, a && a.frameDuration);
                    c.array = [this];var b = setTimeout(function () {
                        c.start();clearTimeout(b);b = null;
                    }, 0);
                }
            }, teardown: function teardown() {
                for (var a = c.array, b = a.length; b--;) {
                    if (a[b] === this) {
                        a.splice(b, 1);break;
                    }
                }if (a.length === 0) {
                    c.stop();c = h;
                }
            }, handler: function handler() {
                d.event.handle.apply(this, arguments);
            } };
    })(jQuery);
}

// jquery.jparallax.js
// 1.0
// Stephen Band
//
// Project and documentation site:
// webdev.stephband.info/jparallax/
//
// Repository:
// github.com/stephband/jparallax
//
// Dependencies:
// jquery.event.frame

(function (l, t) {
    function y(i) {
        return this.lib[i];
    }function q(i) {
        return typeof i === "boolean" ? i : !!parseFloat(i);
    }function r(i, b) {
        var k = [q(i.xparallax), q(i.yparallax)];this.ontarget = false;this.decay = i.decay;this.pointer = b || [0.5, 0.5];this.update = function (e, a) {
            if (this.ontarget) this.pointer = e;else if ((!k[0] || u(e[0] - this.pointer[0]) < a[0]) && (!k[1] || u(e[1] - this.pointer[1]) < a[1])) {
                this.ontarget = true;this.pointer = e;
            } else {
                a = [];for (var g = 2; g--;) {
                    if (k[g]) a[g] = e[g] + this.decay * (this.pointer[g] - e[g]);
                }this.pointer = a;
            }
        };
    }
    function z(i, b) {
        var k = this,
            e = i instanceof l ? i : l(i),
            a = [q(b.xparallax), q(b.yparallax)],
            g = 0,
            d;this.pointer = [0, 0];this.active = false;this.activeOutside = b && b.activeOutside || false;this.update = function (h) {
            var j = this.pos,
                c = this.size,
                f = [],
                m = 2;if (g > 0) {
                if (g === 2) {
                    g = 0;if (d) h = d;
                }for (; m--;) {
                    if (a[m]) {
                        f[m] = (h[m] - j[m]) / c[m];f[m] = f[m] < 0 ? 0 : f[m] > 1 ? 1 : f[m];
                    }
                }this.active = true;this.pointer = f;
            } else this.active = false;
        };this.updateSize = function () {
            var h = e.width(),
                j = e.height();k.size = [h, j];k.threshold = [1 / h, 1 / j];
        };this.updatePos = function () {
            var h = e.offset() || { left: 0, top: 0 },
                j = parseInt(e.css("borderLeftWidth")) + parseInt(e.css("paddingLeft")),
                c = parseInt(e.css("borderTopWidth")) + parseInt(e.css("paddingTop"));k.pos = [h.left + j, h.top + c];
        };l(window).bind("resize", k.updateSize).bind("resize", k.updatePos);e.bind("mouseenter", function () {
            g = 1;
        }).bind("mouseleave", function (h) {
            g = 2;d = [h.pageX, h.pageY];
        });this.updateSize();this.updatePos();
    }function A(i, b) {
        var k = [],
            e = [],
            a = [],
            g = [];this.update = function (d) {
            for (var h = [], j, c, f = 2, m = {}; f--;) {
                if (e[f]) {
                    h[f] = e[f] * d[f] + a[f];
                    if (k[f]) {
                        j = g[f];c = h[f] * -1;
                    } else {
                        j = h[f] * 100 + "%";c = h[f] * this.size[f] * -1;
                    }if (f === 0) {
                        m.left = j;m.marginLeft = c;
                    } else {
                        m.top = j;m.marginTop = c;
                    }
                }
            }i.css(m);
        };this.setParallax = function (d, h, j, c) {
            d = [d || b.xparallax, h || b.yparallax];j = [j || b.xorigin, c || b.yorigin];for (c = 2; c--;) {
                k[c] = o.px.test(d[c]);if (typeof j[c] === "string") j[c] = o.percent.test(j[c]) ? parseFloat(j[c]) / 100 : v[j[c]] || 1;if (k[c]) {
                    e[c] = parseInt(d[c]);a[c] = j[c] * (this.size[c] - e[c]);g[c] = j[c] * 100 + "%";
                } else {
                    e[c] = d[c] === true ? 1 : o.percent.test(d[c]) ? parseFloat(d[c]) / 100 : d[c];a[c] = e[c] ? j[c] * (1 - e[c]) : 0;
                }
            }
        };this.getPointer = function () {
            for (var d = i.offsetParent(), h = i.position(), j = [], c = [], f = 2; f--;) {
                j[f] = k[f] ? 0 : h[f === 0 ? "left" : "top"] / (d[f === 0 ? "outerWidth" : "outerHeight"]() - this.size[f]);c[f] = (j[f] - a[f]) / e[f];
            }return c;
        };this.setSize = function (d, h) {
            this.size = [d || i.outerWidth(), h || i.outerHeight()];
        };this.setSize(b.width, b.height);this.setParallax(b.xparallax, b.yparallax, b.xorigin, b.yorigin);
    }function s(i) {
        var b = l(this),
            k = i.data,
            e = b.data(n),
            a = k.port,
            g = k.mouse,
            d = e.mouse;if (k.timeStamp !== i.timeStamp) {
            k.timeStamp = i.timeStamp;a.update(w);if (a.active || !g.ontarget) g.update(a.pointer, a.threshold);
        }if (d) {
            d.update(e.freeze ? e.freeze.pointer : a.pointer, a.threshold);if (d.ontarget) {
                delete e.mouse;e.freeze && b.unbind(p).addClass(k.freezeClass);
            }g = d;
        } else g.ontarget && !a.active && b.unbind(p);e.layer.update(g.pointer);
    }var n = "parallax",
        x = { mouseport: "body", xparallax: true, yparallax: true, xorigin: 0.5, yorigin: 0.5, decay: 0.66, frameDuration: 30, freezeClass: "freeze" },
        v = { left: 0, top: 0, middle: 0.5, center: 0.5, right: 1,
        bottom: 1 },
        o = { px: /^\d+\s?px$/, percent: /^\d+\s?%$/ },
        p = "frame." + n,
        u = Math.abs,
        w = [0, 0];y.lib = v;l.fn[n] = function (i) {
        var b = l.extend({}, l.fn[n].options, i),
            k = arguments,
            e = this;if (!(b.mouseport instanceof l)) b.mouseport = l(b.mouseport);b.port = new z(b.mouseport, b);b.mouse = new r(b);b.mouseport.bind("mouseenter", function () {
            b.mouse.ontarget = false;e.each(function () {
                var a = l(this);a.data(n).freeze || a.bind(p, b, s);
            });
        });return e.bind("freeze", function (a) {
            var g = l(this),
                d = g.data(n),
                h = d.mouse || d.freeze || b.mouse,
                j = o.percent.exec(a.x) ? parseFloat(a.x.replace(/%$/, "")) / 100 : a.x || h.pointer[0],
                c = o.percent.exec(a.y) ? parseFloat(a.y.replace(/%$/, "")) / 100 : a.y || h.pointer[1];a = a.decay;d.freeze = { pointer: [j, c] };d.mouse = new r(b, h.pointer);if (a !== t) d.mouse.decay = a;g.bind(p, b, s);
        }).bind("unfreeze", function (a) {
            var g = l(this),
                d = g.data(n);a = a.decay;var h;if (d.freeze) {
                h = d.mouse ? d.mouse.pointer : d.freeze.pointer;d.mouse = new r(b);d.mouse.pointer = h;if (a !== t) d.mouse.decay = a;delete d.freeze;g.removeClass(x.freezeClass).bind(p, b, s);
            }
        }).each(function (a) {
            var g = l(this);a = k[a + 1] ? l.extend({}, b, k[a + 1]) : b;var d = new A(g, a);g.data(n, { layer: d, mouse: new r(a, d.getPointer()) });
        });
    };l.fn[n].options = x;l(document).ready(function () {
        l(document).mousemove(function (i) {
            w = [i.pageX, i.pageY];
        });
    });
})(jQuery);
'use strict';

var key = 'ruso_q_712899_example';
var body = document.querySelector('body');

body.classList = window.localStorage[key];

window.addEventListener('storage', function (event) {
    if (event.key !== key) {
        return; // Если прислали не наши данные, ничего не делаем
    }
    body.classList = window.localStorage[key];
});
'use strict';

(function () {
    var body = document.querySelector('body');
    var headerLogoRed = document.querySelector('.header__logo').querySelector('.logo__red');
    var headerLogoBlue = document.querySelector('.header__logo').querySelector('.logo__blue');
    var selectedClubOptions = document.querySelector('.selected-club').querySelectorAll('option');
    var footerLogoRed = document.querySelector('.logo-footer-red');
    var footerLogoBlue = document.querySelector('.logo-footer-blue');

    if (body.className === "red") {
        headerLogoRed.style.display = "";
        headerLogoBlue.style.display = "none";
        selectedClubOptions[0].setAttribute("selected", true);
        footerLogoRed.style.display = "";
        footerLogoBlue.style.display = "none";
        var redColor = document.querySelectorAll('.blue-gradient-title');
        for (var i = 0; i < redColor.length; i++) {
            redColor[i].classList.remove('blue-gradient-title');
            redColor[i].classList.add('red-gradient-title');
        }
    } else {
        headerLogoRed.style.display = "none";
        headerLogoBlue.style.display = "";
        selectedClubOptions[1].setAttribute("selected", true);
        footerLogoRed.style.display = "none";
        footerLogoBlue.style.display = "";
        var blueColor = document.querySelectorAll('.red-gradient-title');
        for (var i = 0; i < blueColor.length; i++) {
            blueColor[i].classList.remove('red-gradient-title');
            blueColor[i].classList.add('blue-gradient-title');
        }
    }

    $(document).ready(function () {
        if (document.querySelector('.promo-code__check') !== null) {
            document.querySelector('.promo-code__check').addEventListener('click', function () {
                setTimeout(function () {
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText = "Отправить";
                    document.querySelector('.promotional-code').querySelector('.captcha').replaceChild(newElement, document.querySelector('.promotional-code').querySelector('.btn__form__captcha'));
                }, 1000);
            });
        }
    });

    $(document).ready(function () {
        if (document.querySelector('.freeze-the-map__modal__check') !== null) {
            document.querySelector('.freeze-the-map__modal__check').addEventListener('click', function () {
                setTimeout(function () {
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText = "Заморозить карту";
                    document.querySelector('.freeze-the-map__modal').querySelector('.captcha').replaceChild(newElement, document.querySelector('.freeze-the-map__modal').querySelector('.btn__form__captcha'));
                }, 1000);
            });
        }
    });

    $(document).ready(function () {
        if (document.querySelector('.request-a-call__modal__check') !== null) {
            document.querySelector('.request-a-call__modal__check').addEventListener('click', function () {
                setTimeout(function () {
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText = "Заказать звонок";
                    document.querySelector('.request-a-call__modal').querySelector('.captcha').replaceChild(newElement, document.querySelector('.request-a-call__modal').querySelector('.btn__form__captcha'));
                }, 1000);
            });
        }
    });

    $(document).ready(function () {
        if (document.querySelector('.events-pages__check') !== null) {
            document.querySelector('.events-pages__check').addEventListener('click', function () {
                setTimeout(function () {
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText = "Отправить";
                    document.querySelector('.events-pages-form__flex').querySelector('.captcha').replaceChild(newElement, document.querySelector('.events-pages-form__flex').querySelector('.btn__form__captcha'));
                }, 1000);
            });
        }
    });

    $(document).ready(function () {
        if (document.querySelector('.comments-and-suggestions__check') !== null) {
            document.querySelector('.comments-and-suggestions__check').addEventListener('click', function () {
                setTimeout(function () {
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText = "Отправить";
                    document.querySelector('.comments-and-suggestions-form').querySelector('.captcha').replaceChild(newElement, document.querySelector('.comments-and-suggestions-form').querySelector('.btn__form__captcha'));
                }, 1000);
            });
        }
    });

    $(document).ready(function () {
        if (document.querySelector('.advertising-and-partners__checkbox') !== null) {
            document.querySelector('.advertising-and-partners__checkbox').addEventListener('click', function () {
                setTimeout(function () {
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText = "Отправить";
                    document.querySelector('.advertising-and-partners__blocks__right').querySelector('.captcha').replaceChild(newElement, document.querySelector('.advertising-and-partners__blocks__right').querySelector('.btn__form__captcha'));
                }, 1000);
            });
        }
    });

    $(document).ready(function () {
        if (document.querySelector('.club-card__modal__check') !== null) {
            document.querySelector('.club-card__modal__check').addEventListener('click', function () {
                setTimeout(function () {
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText = "Оформить карту";
                    document.querySelector('.club-card__modal').querySelector('.captcha').replaceChild(newElement, document.querySelector('.club-card__modal').querySelector('.btn__form__captcha'));
                }, 1000);
            });
        }
    });

    $(document).ready(function () {
        if (document.querySelector('.get-personal-training__modal__check') !== null) {
            document.querySelector('.get-personal-training__modal__check').addEventListener('click', function () {
                setTimeout(function () {
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText = "Отправить";
                    document.querySelector('.get-personal-training__modal').querySelector('.captcha').replaceChild(newElement, document.querySelector('.get-personal-training__modal').querySelector('.btn__form__captcha'));
                }, 1000);
            });
        }
    });

    $(document).ready(function () {
        if (document.querySelector('.buy-online__modal__check') !== null) {
            document.querySelector('.buy-online__modal__check').addEventListener('click', function () {
                setTimeout(function () {
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText = "Оплатить";
                    document.querySelector('.buy-online__modal').querySelector('.captcha').replaceChild(newElement, document.querySelector('.buy-online__modal').querySelector('.btn__form__captcha'));
                }, 1000);
            });
        }
    });

    $(document).ready(function () {
        if (document.querySelector('.childrens__room__modal__check') !== null) {
            document.querySelector('.childrens__room__modal__check').addEventListener('click', function () {
                setTimeout(function () {
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText = "Оплатить";
                    document.querySelector('.childrens__room__modal').querySelector('.captcha').replaceChild(newElement, document.querySelector('.childrens__room__modal').querySelector('.btn__form__captcha'));
                }, 1000);
            });
        }
    });

    $(document).ready(function () {
        if (document.querySelector('.tanning__studio__modal__check') !== null) {
            document.querySelector('.tanning__studio__modal__check').addEventListener('click', function () {
                setTimeout(function () {
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText = "Оплатить";
                    document.querySelector('.tanning__studio__modal').querySelector('.captcha').replaceChild(newElement, document.querySelector('.tanning__studio__modal').querySelector('.btn__form__captcha'));
                }, 1000);
            });
        }
    });

    $(document).ready(function () {
        if (document.querySelector('.gift-certificates__modal__check') !== null) {
            document.querySelector('.gift-certificates__modal__check').addEventListener('click', function () {
                setTimeout(function () {
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText = "Оплатить";
                    document.querySelector('.gift-certificates__modal').querySelector('.captcha').replaceChild(newElement, document.querySelector('.gift-certificates__modal').querySelector('.btn__form__captcha'));
                }, 1000);
            });
        }
    });

    if (window.matchMedia("(max-width: 767px)").matches) {
        var btn = document.querySelectorAll('.btn__container');
        var container = document.querySelectorAll('.contacts-map__container');
        for (var i = 0; i < btn.length; i++) {
            btn[i].style.display = "";
            container[i].style.display = "none";
        }
        $(btn).on('click', function (evt) {
            for (var i = 0; i < btn.length; i++) {
                if (evt.target.className === btn[i].className) {
                    btn[i].style.display = "none";
                    container[i].style.display = "";
                }
            }
        });
    }

    var modalClose = document.querySelectorAll('.modal__close');
    var modal = document.querySelectorAll('.modal');

    $(window).on("click", function (evt) {
        for (var i = 0; i < modal.length; i++) {
            if (modal[i].style.display !== "none" && evt.target === modal[i]) {
                modal[i].style.display = "none";
            }
        }
    });

    $(modalClose).on("click", function (evt) {
        for (var i = 0; i < modalClose.length; i++) {
            if (modalClose[i].className === evt.target.className) {
                modal[i].style.display = "none";
            }
        }
    });

    $(document.querySelector('.get-personal-training__modal__close')).on("click", function () {
        document.querySelector('.get-personal-training__modal__close').style.display = "none";
        document.querySelector('.trainers__modal').style.display = "";
    });

    $(document.querySelector('.btn__footer')).on("click", function () {
        document.querySelector('.freeze-the-map__modal').style.display = "";
    });

    $(document.querySelector('.freeze-the-map')).on("click", function () {
        document.querySelector('.freeze-the-map__modal').style.display = "";
    });

    $(document.querySelector('.header__call')).on("click", function () {
        document.querySelector('.request-a-call__modal').style.display = "";
    });

    if (document.querySelector('.club-card__btn') !== null) {
        $(document.querySelectorAll('.club-card__btn')).on("click", function () {
            document.querySelector('.club-card__modal').style.display = "";
        });
    }

    if (document.querySelector('.success-stories___block') !== null) {
        $(document.querySelectorAll('.success-stories___block')).on("click", function () {
            document.querySelector('.success-stories__modal').style.display = "";
        });
    }

    $(document.querySelector('.trainers__modal__btn')).on("click", function () {
        document.querySelector('.get-personal-training__modal').style.display = "";
        document.querySelector('.trainers__modal').style.display = "none";
    });

    $(document.querySelector('.buy-online__btn')).on("click", function () {
        document.querySelector('.buy-online__modal').style.display = "";
    });

    $(document.querySelector('.more-about-the-map__btn')).on("click", function () {
        document.querySelector('.more-about-the-map__modal').style.display = "";
    });

    $(document.querySelector('.childrens__room__btn')).on("click", function () {
        document.querySelector('.childrens__room__modal').style.display = "";
    });

    $(document.querySelector('.tanning__studio__btn')).on("click", function () {
        document.querySelector('.tanning__studio__modal').style.display = "";
    });

    $(document.querySelectorAll('.buy-online')).on("click", function () {
        document.querySelector('.gift-certificates__modal').style.display = "";
    });

    //adaptive-menu

    if (window.matchMedia("(max-width: 1366px) and (min-width: 768px)").matches) {
        $(document.querySelectorAll('.header__menu__adaptiv__up')).on("click", function () {
            document.querySelector('.menu-adaptive').style.display = "flex";
            document.querySelector('.header__search').style.display = "block";
            document.querySelector('.selected-club').style.display = "block";
        });

        $('.menu-adaptive__close').on("click", function () {
            document.querySelector('.menu-adaptive').style.display = "none";
            document.querySelector('.header__search').style.display = "none";
            document.querySelector('.selected-club').style.display = "none";
        });

        $('.header__search').on("click", function () {
            document.querySelector('.search__form__header').style.display = "flex";
        });

        $('.search-form__close').on("click", function () {
            document.querySelector('.search__form__header').style.display = "none";
        });

        $('.submenu__1').on("click", function (evt) {
            document.querySelector('.link__1').style.display = "block";
            document.querySelector('.menu-main').classList = 'menu-main active__main';
            document.querySelector('.submenu__1').querySelector('.menu-main__link').classList = 'menu-main__link active__menu__link';
            if (document.querySelector('.link__2').style.display === "block") {
                document.querySelector('.link__2').style.display = "none";
                document.querySelector('.submenu__2').querySelector('.menu-main__link').classList = 'menu-main__link';
            }
            evt.preventDefault();
        });

        $('.submenu__2').on("click", function (evt) {
            document.querySelector('.link__2').style.display = "block";
            document.querySelector('.menu-main').classList = 'menu-main active__main';
            document.querySelector('.submenu__2').querySelector('.menu-main__link').classList = 'menu-main__link active__menu__link';
            if (document.querySelector('.link__1').style.display === "block") {
                document.querySelector('.link__1').style.display = "none";
                document.querySelector('.submenu__1').querySelector('.menu-main__link').classList = 'menu-main__link';
            }
            evt.preventDefault();
        });
    }

    if (window.matchMedia("(max-width: 767px)").matches) {
        $(document.querySelectorAll('.header__menu__adaptiv__up')).on("click", function () {
            document.querySelector('.menu-adaptive').style.display = "flex";
            document.querySelector('.header__search').style.display = "block";
            document.querySelector('.selected-club').style.display = "block";
        });

        $('.menu-adaptive__close').on("click", function () {
            document.querySelector('.menu-adaptive').style.display = "none";
            document.querySelector('.header__search').style.display = "none";
            document.querySelector('.selected-club').style.display = "none";
        });

        $('.header__search').on("click", function () {
            document.querySelector('.search__form__header').style.display = "flex";
        });

        $('.search-form__close').on("click", function () {
            document.querySelector('.search__form__header').style.display = "none";
        });

        $('.submenu__1').on("click", function (evt) {
            document.querySelector('.link__1').style.display = "block";
            document.querySelector('.menu-main').style.display = "none";
            evt.preventDefault();
        });

        $('.submenu__2').on("click", function (evt) {
            document.querySelector('.link__2').style.display = "block";
            document.querySelector('.menu-main').style.display = "none";
            evt.preventDefault();
        });

        $('.back__1').on("click", function () {
            document.querySelector('.link__1').style.display = "none";
            document.querySelector('.menu-main').style.display = "flex";
        });

        $('.back__2').on("click", function () {
            document.querySelector('.link__2').style.display = "none";
            document.querySelector('.menu-main').style.display = "flex";
        });
    }
})();
"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

!function (a) {
  "function" == typeof define && define.amd ? define(["jquery"], a) : a("object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) ? require("jquery") : jQuery);
}(function (a) {
  var b,
      c = navigator.userAgent,
      d = /iphone/i.test(c),
      e = /chrome/i.test(c),
      f = /android/i.test(c);a.mask = { definitions: { 9: "[0-9]", a: "[A-Za-z]", "*": "[A-Za-z0-9]" }, autoclear: !0, dataName: "rawMaskFn", placeholder: "_" }, a.fn.extend({ caret: function caret(a, b) {
      var c;if (0 !== this.length && !this.is(":hidden")) return "number" == typeof a ? (b = "number" == typeof b ? b : a, this.each(function () {
        this.setSelectionRange ? this.setSelectionRange(a, b) : this.createTextRange && (c = this.createTextRange(), c.collapse(!0), c.moveEnd("character", b), c.moveStart("character", a), c.select());
      })) : (this[0].setSelectionRange ? (a = this[0].selectionStart, b = this[0].selectionEnd) : document.selection && document.selection.createRange && (c = document.selection.createRange(), a = 0 - c.duplicate().moveStart("character", -1e5), b = a + c.text.length), { begin: a, end: b });
    }, unmask: function unmask() {
      return this.trigger("unmask");
    }, mask: function mask(c, g) {
      var h, i, j, k, l, m, n, o;if (!c && this.length > 0) {
        h = a(this[0]);var p = h.data(a.mask.dataName);return p ? p() : void 0;
      }return g = a.extend({ autoclear: a.mask.autoclear, placeholder: a.mask.placeholder, completed: null }, g), i = a.mask.definitions, j = [], k = n = c.length, l = null, a.each(c.split(""), function (a, b) {
        "?" == b ? (n--, k = a) : i[b] ? (j.push(new RegExp(i[b])), null === l && (l = j.length - 1), k > a && (m = j.length - 1)) : j.push(null);
      }), this.trigger("unmask").each(function () {
        function h() {
          if (g.completed) {
            for (var a = l; m >= a; a++) {
              if (j[a] && C[a] === p(a)) return;
            }g.completed.call(B);
          }
        }function p(a) {
          return g.placeholder.charAt(a < g.placeholder.length ? a : 0);
        }function q(a) {
          for (; ++a < n && !j[a];) {}return a;
        }function r(a) {
          for (; --a >= 0 && !j[a];) {}return a;
        }function s(a, b) {
          var c, d;if (!(0 > a)) {
            for (c = a, d = q(b); n > c; c++) {
              if (j[c]) {
                if (!(n > d && j[c].test(C[d]))) break;C[c] = C[d], C[d] = p(d), d = q(d);
              }
            }z(), B.caret(Math.max(l, a));
          }
        }function t(a) {
          var b, c, d, e;for (b = a, c = p(a); n > b; b++) {
            if (j[b]) {
              if (d = q(b), e = C[b], C[b] = c, !(n > d && j[d].test(e))) break;c = e;
            }
          }
        }function u() {
          var a = B.val(),
              b = B.caret();if (o && o.length && o.length > a.length) {
            for (A(!0); b.begin > 0 && !j[b.begin - 1];) {
              b.begin--;
            }if (0 === b.begin) for (; b.begin < l && !j[b.begin];) {
              b.begin++;
            }B.caret(b.begin, b.begin);
          } else {
            for (A(!0); b.begin < n && !j[b.begin];) {
              b.begin++;
            }B.caret(b.begin, b.begin);
          }h();
        }function v() {
          A(), B.val() != E && B.change();
        }function w(a) {
          if (!B.prop("readonly")) {
            var b,
                c,
                e,
                f = a.which || a.keyCode;o = B.val(), 8 === f || 46 === f || d && 127 === f ? (b = B.caret(), c = b.begin, e = b.end, e - c === 0 && (c = 46 !== f ? r(c) : e = q(c - 1), e = 46 === f ? q(e) : e), y(c, e), s(c, e - 1), a.preventDefault()) : 13 === f ? v.call(this, a) : 27 === f && (B.val(E), B.caret(0, A()), a.preventDefault());
          }
        }function x(b) {
          if (!B.prop("readonly")) {
            var c,
                d,
                e,
                g = b.which || b.keyCode,
                i = B.caret();if (!(b.ctrlKey || b.altKey || b.metaKey || 32 > g) && g && 13 !== g) {
              if (i.end - i.begin !== 0 && (y(i.begin, i.end), s(i.begin, i.end - 1)), c = q(i.begin - 1), n > c && (d = String.fromCharCode(g), j[c].test(d))) {
                if (t(c), C[c] = d, z(), e = q(c), f) {
                  var k = function k() {
                    a.proxy(a.fn.caret, B, e)();
                  };setTimeout(k, 0);
                } else B.caret(e);i.begin <= m && h();
              }b.preventDefault();
            }
          }
        }function y(a, b) {
          var c;for (c = a; b > c && n > c; c++) {
            j[c] && (C[c] = p(c));
          }
        }function z() {
          B.val(C.join(""));
        }function A(a) {
          var b,
              c,
              d,
              e = B.val(),
              f = -1;for (b = 0, d = 0; n > b; b++) {
            if (j[b]) {
              for (C[b] = p(b); d++ < e.length;) {
                if (c = e.charAt(d - 1), j[b].test(c)) {
                  C[b] = c, f = b;break;
                }
              }if (d > e.length) {
                y(b + 1, n);break;
              }
            } else C[b] === e.charAt(d) && d++, k > b && (f = b);
          }return a ? z() : k > f + 1 ? g.autoclear || C.join("") === D ? (B.val() && B.val(""), y(0, n)) : z() : (z(), B.val(B.val().substring(0, f + 1))), k ? b : l;
        }var B = a(this),
            C = a.map(c.split(""), function (a, b) {
          return "?" != a ? i[a] ? p(b) : a : void 0;
        }),
            D = C.join(""),
            E = B.val();B.data(a.mask.dataName, function () {
          return a.map(C, function (a, b) {
            return j[b] && a != p(b) ? a : null;
          }).join("");
        }), B.one("unmask", function () {
          B.off(".mask").removeData(a.mask.dataName);
        }).on("focus.mask", function () {
          if (!B.prop("readonly")) {
            clearTimeout(b);var a;E = B.val(), a = A(), b = setTimeout(function () {
              B.get(0) === document.activeElement && (z(), a == c.replace("?", "").length ? B.caret(0, a) : B.caret(a));
            }, 10);
          }
        }).on("blur.mask", v).on("keydown.mask", w).on("keypress.mask", x).on("input.mask paste.mask", function () {
          B.prop("readonly") || setTimeout(function () {
            var a = A(!0);B.caret(a), h();
          }, 0);
        }), e && f && B.off("input.mask").on("input.mask", u), A();
      });
    } });
});
"use strict";

(function () {
    var sliderMaximusTop = document.querySelector('.slider__maximus__top').querySelectorAll('img');
    var sliderMaximusTopContentsLeft = document.querySelector('.slider__maximus__top__contents__left').querySelectorAll('span');
    var sliderMaximusTopContentsRight = document.querySelector('.slider__maximus__top__contents__right').querySelectorAll('div');

    $(sliderMaximusTopContentsLeft).on("click", function (evt) {
        var sliderMaximusTopImgActive = document.querySelector('.slider__maximus__top').querySelector('.active__img');
        var sliderMaximusTopContentsLeftActive = document.querySelector('.slider__maximus__top__contents__left').querySelector('.active__block');
        var sliderMaximusTopContentsRightActive = document.querySelector('.slider__maximus__top__contents__right').querySelector('.active__info');

        for (var i = 0; i < sliderMaximusTop.length; i++) {
            if (sliderMaximusTopContentsLeft[i].innerText === evt.target.innerText) {
                sliderMaximusTopImgActive.classList.remove('active__img');
                sliderMaximusTop[i].classList = 'active active__img';
                sliderMaximusTopImgActive.style.display = "none";
                sliderMaximusTop[i].style.display = "";

                sliderMaximusTopContentsLeftActive.classList.remove('active__block');
                sliderMaximusTopContentsLeftActive.classList.remove('active');
                sliderMaximusTopContentsLeft[i].classList = 'active active__block';

                sliderMaximusTopContentsRightActive.classList.remove('active__info');
                sliderMaximusTopContentsRight[i].classList = 'active__info';
                sliderMaximusTopContentsRightActive.style.display = "none";
                sliderMaximusTopContentsRight[i].style.display = "";
            }
        }
    });

    var sliderMaximusBottom = document.querySelector('.slider__maximus__bottom').querySelectorAll('img');
    var sliderMaximusBottomContentsLeft = document.querySelector('.slider__maximus__bottom__contents__left').querySelectorAll('span');
    var sliderMaximusBottomContentsRight = document.querySelector('.slider__maximus__bottom__contents__right').querySelectorAll('.info');

    $(sliderMaximusBottomContentsLeft).on("click", function (evt) {
        var sliderMaximusBottomImgActive = document.querySelector('.slider__maximus__bottom').querySelector('.active__img');
        var sliderMaximusBottomContentsLeftActive = document.querySelector('.slider__maximus__bottom__contents__left').querySelector('.active__block');
        var sliderMaximusBottomContentsRightActive = document.querySelector('.slider__maximus__bottom__contents__right').querySelector('.active__info');

        for (var i = 0; i < sliderMaximusBottom.length; i++) {
            if (sliderMaximusBottomContentsLeft[i].innerText === evt.target.innerText) {
                sliderMaximusBottomImgActive.classList.remove('active__img');
                sliderMaximusBottom[i].classList = 'active active__img';
                sliderMaximusBottomImgActive.style.display = "none";
                sliderMaximusBottom[i].style.display = "";

                sliderMaximusBottomContentsLeftActive.classList.remove('active__block');
                sliderMaximusBottomContentsLeftActive.classList.remove('active');
                sliderMaximusBottomContentsLeft[i].classList = 'active active__block';

                sliderMaximusBottomContentsRightActive.classList.remove('active__info');
                sliderMaximusBottomContentsRight[i].classList = 'info active__info';
                sliderMaximusBottomContentsRightActive.style.display = "none";
                sliderMaximusBottomContentsRight[i].style.display = "";
            }
        }
    });
})();
'use strict';

(function () {
    var btnSliders = document.querySelectorAll(".btn__sliders");
    var slide = document.querySelectorAll(".slide");

    $(btnSliders).on("click", function (evt) {
        var slideActive = document.querySelector(".active__slides");
        var btnActive = document.querySelector(".active__slide");
        for (var i = 0; i < slide.length; i++) {
            if (btnSliders[i].className === evt.target.className && btnActive.className !== evt.target.className) {
                slide[i].style.display = "";
                slideActive.style.display = "none";
                slide[i].classList.add("active__slides");
                slideActive.classList.remove("active__slides");
                btnSliders[i].classList.add("active__slide");
                btnActive.classList.remove("active__slide");
            }
        }
    });
})();
'use strict';

(function () {
    var vacansiesConditions = document.querySelector('.vacansies__conditions');
    var vacansiesAll = document.querySelector('.vacansies__all');
    var jobTitle = document.querySelectorAll('.vacansies__job-title');
    var jobBlock = document.querySelectorAll('.vacansies__job-block');
    var btn = document.querySelectorAll('.vacansies__btn');
    var vacansiesRespond = document.querySelectorAll('.vacansies__respond');

    $(jobTitle).on("click", function () {
        vacansiesConditions.style.display = "none";
        ths = this;
        for (var i = 0; i < jobTitle.length; i++) {
            if (this.textContent === jobTitle[i].textContent) {
                if (jobBlock[i].style.display + '' === "none") {
                    jobBlock[i].style.display = "";
                } else {
                    jobBlock[i].style.display = "none";
                }
            }
        }
    });

    $(btn).on("click", function () {
        ths = this;
        var active = document.querySelector('.active');
        for (var i = 0; i < btn.length; i++) {
            if (this.value === btn[i].value) {
                if (active !== null) {
                    active.classList = "vacansies__respond";
                    active.style.display = "none";
                }
                vacansiesRespond[i].style.display = "";
                vacansiesRespond[i].classList = "vacansies__respond active";
            }
        }
    });

    if (window.matchMedia("(max-width: 768px)").matches) {
        $(btn).on("click", function () {
            vacansiesAll.style.display = "none";
        });
    }

    function createEl(captcha, inp) {
        var newElement = document.createElement('button');
        newElement.className = 'btn btn__form btn__form__captcha';
        newElement.innerText = "Отправить";
        captcha.replaceChild(newElement, inp);
    }

    $(document).ready(function () {
        if (document.querySelector('.vacansies__check') !== null) {
            var btn = document.querySelectorAll('.vacansies__check');
            $(btn).on('click', function (evt) {
                var btn = document.querySelectorAll('.vacansies__check');
                var inp = document.querySelectorAll('.btn__form__captcha__vacansies');
                var captcha = document.querySelectorAll('.captcha__vacansies');

                for (var i = 0; i < btn.length; i++) {
                    if (evt.target.className === btn[i].className) {
                        setTimeout(createEl(captcha[i], inp[i]), 1000);
                    }
                }
            });
        }
    });
})();
//# sourceMappingURL=script.js.map
