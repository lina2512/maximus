'use strict';

(function () {
    var infoLeft = document.querySelector('.info-left');
    var infoLeftext1 = document.querySelector('.info-left').querySelector('h3');
    var infoLeftext2 = document.querySelector('.info-left').querySelector('p');
    var level1btn = document.querySelector('.constructor__level1').querySelector('.btn');
    var level2btn = document.querySelector('.constructor__level2').querySelectorAll('.constructor__level2__logo');
    var level3btn = document.querySelector('.constructor__level3').querySelectorAll('.constructor__level3__block');
    var level4btn = document.querySelector('.constructor__level4').querySelector('.btn__next');
    var level5btn = document.querySelector('.constructor__level5').querySelectorAll('.card__month');
    var level1 = document.querySelector('.constructor__level1');
    var level2 = document.querySelector('.constructor__level2');
    var level3 = document.querySelector('.constructor__level3');
    var level4 = document.querySelector('.constructor__level4');
    var level5 = document.querySelector('.constructor__level5');
    var level2btnBack = document.querySelector('.constructor__level2').querySelector('.btn');
    var level3btnBack = document.querySelector('.constructor__level3').querySelector('.btn');
    var level4btnBack = document.querySelector('.constructor__level4').querySelector('.btn__back');
    var level5btnBack = document.querySelector('.constructor__level5').querySelector('.btn__back');


    $(level1btn).on("click", function() {
        level1.style.display = "none";
        level2.style.display = "";
    });

    $(level2btnBack).on("click", function() {
        level1.style.display = "";
        level2.style.display = "none";
    });

    $(level2btn).on("click", function() {
        for (var i = 0; i < level2btn.length; i++) {
            if(this.className === level2btn[i].className) {
                infoLeft.removeChild(infoLeftext1);
                infoLeft.removeChild(infoLeftext2);
                var logo = level2btn[i].cloneNode(true);
                infoLeft.appendChild(logo);
            }
        }
        level2.style.display = "none";
        level3.style.display = "";
    });

    $(level3btnBack).on("click", function() {
        level2.style.display = "";
        level3.style.display = "none";
        infoLeft.appendChild(infoLeftext1);
        infoLeft.appendChild(infoLeftext2);
        infoLeft.removeChild(document.querySelector('.info-left').querySelector('.constructor__level2__logo'));
    });

    $(level3btn).on("click", function() {
        for (var i = 0; i < level3btn.length; i++) {
            if(this.className === level3btn[i].className) {
                var time = level3btn[i].cloneNode(true);
                infoLeft.appendChild(time);
            }
        }
        level3.style.display = "none";
        level4.style.display = "";
    });

    $(level4btnBack).on("click", function() {
        level3.style.display = "";
        level4.style.display = "none";
        infoLeft.removeChild(document.querySelector('.info-left').querySelector('.constructor__level3__block'));
    });

    $(level4btn).on("click", function() {
        var checked = document.querySelector('.constructor__level4__all').querySelectorAll('.checkbox');
        var inputChecked = document.querySelector('.constructor__level4__all').querySelectorAll('.checkbox__color__active');
        for (var i = 0; i < checked.length; i++) {
            if ($(checked[i]).prop('checked')) {
                var time = inputChecked[i].cloneNode(true);
                infoLeft.appendChild(time);
            }
        }
        level4.style.display = "none";
        level5.style.display = "";
    });

    $(level5btnBack).on("click", function() {
        level4.style.display = "";
        level5.style.display = "none";
        var inputChecked = document.querySelector('.info-left').querySelectorAll('.checkbox__color__active');
        for (var i = 0; i < inputChecked.length; i++) {
            infoLeft.removeChild(inputChecked[i]);
        }
    });

    $(level5btn).on("click", function() {
        document.querySelector('.constructor').style.display = "none";
        document.querySelector('.selected__card').style.display = "";
    });
})();