"use strict";

(function () {
    var sliderMaximusTop = document.querySelector('.slider__maximus__top').querySelectorAll('img');
    var sliderMaximusTopContentsLeft = document.querySelector('.slider__maximus__top__contents__left').querySelectorAll('span');
    var sliderMaximusTopContentsRight = document.querySelector('.slider__maximus__top__contents__right').querySelectorAll('div');

    $(sliderMaximusTopContentsLeft).on("click", function(evt){
        var sliderMaximusTopImgActive = document.querySelector('.slider__maximus__top').querySelector('.active__img');
        var sliderMaximusTopContentsLeftActive = document.querySelector('.slider__maximus__top__contents__left').querySelector('.active__block');
        var sliderMaximusTopContentsRightActive = document.querySelector('.slider__maximus__top__contents__right').querySelector('.active__info');

        for(var i = 0; i < sliderMaximusTop.length; i++) {
            if(sliderMaximusTopContentsLeft[i].innerText === evt.target.innerText) {
                sliderMaximusTopImgActive.classList.remove('active__img');
                sliderMaximusTop[i].classList = 'active active__img';
                sliderMaximusTopImgActive.style.display = "none";
                sliderMaximusTop[i].style.display = "";

                sliderMaximusTopContentsLeftActive.classList.remove('active__block');
                sliderMaximusTopContentsLeftActive.classList.remove('active');
                sliderMaximusTopContentsLeft[i].classList = 'active active__block';

                sliderMaximusTopContentsRightActive.classList.remove('active__info');
                sliderMaximusTopContentsRight[i].classList = 'active__info';
                sliderMaximusTopContentsRightActive.style.display = "none";
                sliderMaximusTopContentsRight[i].style.display = "";
            }
        }
    });

    var sliderMaximusBottom = document.querySelector('.slider__maximus__bottom').querySelectorAll('img');
    var sliderMaximusBottomContentsLeft = document.querySelector('.slider__maximus__bottom__contents__left').querySelectorAll('span');
    var sliderMaximusBottomContentsRight = document.querySelector('.slider__maximus__bottom__contents__right').querySelectorAll('.info');

    $(sliderMaximusBottomContentsLeft).on("click", function(evt){
        var sliderMaximusBottomImgActive = document.querySelector('.slider__maximus__bottom').querySelector('.active__img');
        var sliderMaximusBottomContentsLeftActive = document.querySelector('.slider__maximus__bottom__contents__left').querySelector('.active__block');
        var sliderMaximusBottomContentsRightActive = document.querySelector('.slider__maximus__bottom__contents__right').querySelector('.active__info');

        for(var i = 0; i < sliderMaximusBottom.length; i++) {
            if(sliderMaximusBottomContentsLeft[i].innerText === evt.target.innerText) {
                sliderMaximusBottomImgActive.classList.remove('active__img');
                sliderMaximusBottom[i].classList = 'active active__img';
                sliderMaximusBottomImgActive.style.display = "none";
                sliderMaximusBottom[i].style.display = "";

                sliderMaximusBottomContentsLeftActive.classList.remove('active__block');
                sliderMaximusBottomContentsLeftActive.classList.remove('active');
                sliderMaximusBottomContentsLeft[i].classList = 'active active__block';

                sliderMaximusBottomContentsRightActive.classList.remove('active__info');
                sliderMaximusBottomContentsRight[i].classList = 'info active__info';
                sliderMaximusBottomContentsRightActive.style.display = "none";
                sliderMaximusBottomContentsRight[i].style.display = "";
            }
        }
    });

})();