const btnRed = document.querySelector('.landing__red').querySelector('.btn__red__a');
const btnBlue = document.querySelector('.landing__blue').querySelector('.btn__blue__a');

const logoRed = document.querySelector('.landing__red').querySelector('.logo__red__a');
const logoBlue = document.querySelector('.landing__blue').querySelector('.logo__blue__a');

const body = document.querySelector('body');


btnBlue.addEventListener('click', handleUpdateBlue);
btnRed.addEventListener('click', handleUpdateRed);
logoBlue.addEventListener('click', handleUpdateBlue);
logoRed.addEventListener('click', handleUpdateRed);

var key = 'ruso_q_712899_example';

function handleUpdateRed() {
    window.localStorage[key] = "red";
    body.classList = "red";
}

function handleUpdateBlue() {
    window.localStorage[key] = "blue";
    body.classList = "blue";
}