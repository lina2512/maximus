'use strict';

(function (){
    const body = document.querySelector('body');
    var headerLogoRed = document.querySelector('.header__logo').querySelector('.logo__red');
    var headerLogoBlue = document.querySelector('.header__logo').querySelector('.logo__blue');
    var selectedClubOptions = document.querySelector('.selected-club').querySelectorAll('option');
    var footerLogoRed = document.querySelector('.logo-footer-red');
    var footerLogoBlue = document.querySelector('.logo-footer-blue');

    if (body.className === "red") {
        headerLogoRed.style.display = "";
        headerLogoBlue.style.display = "none";
        selectedClubOptions[0].setAttribute("selected", true);
        footerLogoRed.style.display = "";
        footerLogoBlue.style.display = "none";
        var redColor = document.querySelectorAll('.blue-gradient-title');
        for(var i = 0; i < redColor.length; i++) {
            redColor[i].classList.remove('blue-gradient-title');
            redColor[i].classList.add('red-gradient-title');
        }
    } else {
        headerLogoRed.style.display = "none";
        headerLogoBlue.style.display = "";
        selectedClubOptions[1].setAttribute("selected", true);
        footerLogoRed.style.display = "none";
        footerLogoBlue.style.display = "";
        var blueColor = document.querySelectorAll('.red-gradient-title');
        for(var i = 0; i < blueColor.length; i++) {
            blueColor[i].classList.remove('red-gradient-title');
            blueColor[i].classList.add('blue-gradient-title');
        }
    }

    $(document).ready(function(){
        if(document.querySelector('.promo-code__check') !== null) {
            document.querySelector('.promo-code__check').addEventListener('click', function(){
                setTimeout(function(){
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText ="Отправить";
                    document.querySelector('.promotional-code').querySelector('.captcha').replaceChild(newElement, document.querySelector('.promotional-code').querySelector('.btn__form__captcha'));
                },1000);
            });
        }
    });

    $(document).ready(function(){
        if(document.querySelector('.freeze-the-map__modal__check') !== null) {
            document.querySelector('.freeze-the-map__modal__check').addEventListener('click', function () {
                setTimeout(function () {
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText = "Заморозить карту";
                    document.querySelector('.freeze-the-map__modal').querySelector('.captcha').replaceChild(newElement, document.querySelector('.freeze-the-map__modal').querySelector('.btn__form__captcha'));
                }, 1000);
            });
        }
    });

    $(document).ready(function(){
        if(document.querySelector('.request-a-call__modal__check') !== null) {
            document.querySelector('.request-a-call__modal__check').addEventListener('click', function () {
                setTimeout(function () {
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText = "Заказать звонок";
                    document.querySelector('.request-a-call__modal').querySelector('.captcha').replaceChild(newElement, document.querySelector('.request-a-call__modal').querySelector('.btn__form__captcha'));
                }, 1000);
            });
        }
    });


    $(document).ready(function(){
        if(document.querySelector('.events-pages__check') !== null) {
            document.querySelector('.events-pages__check').addEventListener('click', function(){
                setTimeout(function(){
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText ="Отправить";
                    document.querySelector('.events-pages-form__flex').querySelector('.captcha').replaceChild(newElement, document.querySelector('.events-pages-form__flex').querySelector('.btn__form__captcha'));
                },1000);
            });
        }
    });

    $(document).ready(function(){
        if(document.querySelector('.comments-and-suggestions__check') !== null) {
            document.querySelector('.comments-and-suggestions__check').addEventListener('click', function(){
                setTimeout(function(){
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText ="Отправить";
                    document.querySelector('.comments-and-suggestions-form').querySelector('.captcha').replaceChild(newElement, document.querySelector('.comments-and-suggestions-form').querySelector('.btn__form__captcha'));
                },1000);
            });
        }
    });

    $(document).ready(function(){
        if(document.querySelector('.advertising-and-partners__checkbox') !== null) {
            document.querySelector('.advertising-and-partners__checkbox').addEventListener('click', function(){
                setTimeout(function(){
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText ="Отправить";
                    document.querySelector('.advertising-and-partners__blocks__right').querySelector('.captcha').replaceChild(newElement,
                        document.querySelector('.advertising-and-partners__blocks__right').querySelector('.btn__form__captcha'));
                },1000);
            });
        }
    });

    $(document).ready(function(){
        if(document.querySelector('.club-card__modal__check') !== null) {
            document.querySelector('.club-card__modal__check').addEventListener('click', function(){
                setTimeout(function(){
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText ="Оформить карту";
                    document.querySelector('.club-card__modal').querySelector('.captcha').replaceChild(newElement,
                        document.querySelector('.club-card__modal').querySelector('.btn__form__captcha'));
                },1000);
            });
        }
    });

    $(document).ready(function(){
        if(document.querySelector('.get-personal-training__modal__check') !== null) {
            document.querySelector('.get-personal-training__modal__check').addEventListener('click', function(){
                setTimeout(function(){
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText ="Отправить";
                    document.querySelector('.get-personal-training__modal').querySelector('.captcha').replaceChild(newElement,
                        document.querySelector('.get-personal-training__modal').querySelector('.btn__form__captcha'));
                },1000);
            });
        }
    });

    $(document).ready(function(){
        if(document.querySelector('.buy-online__modal__check') !== null) {
            document.querySelector('.buy-online__modal__check').addEventListener('click', function(){
                setTimeout(function(){
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText ="Оплатить";
                    document.querySelector('.buy-online__modal').querySelector('.captcha').replaceChild(newElement,
                        document.querySelector('.buy-online__modal').querySelector('.btn__form__captcha'));
                },1000);
            });
        }
    });

    $(document).ready(function(){
        if(document.querySelector('.childrens__room__modal__check') !== null) {
            document.querySelector('.childrens__room__modal__check').addEventListener('click', function(){
                setTimeout(function(){
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText ="Оплатить";
                    document.querySelector('.childrens__room__modal').querySelector('.captcha').replaceChild(newElement,
                        document.querySelector('.childrens__room__modal').querySelector('.btn__form__captcha'));
                },1000);
            });
        }
    });

    $(document).ready(function(){
        if(document.querySelector('.tanning__studio__modal__check') !== null) {
            document.querySelector('.tanning__studio__modal__check').addEventListener('click', function(){
                setTimeout(function(){
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText ="Оплатить";
                    document.querySelector('.tanning__studio__modal').querySelector('.captcha').replaceChild(newElement,
                        document.querySelector('.tanning__studio__modal').querySelector('.btn__form__captcha'));
                },1000);
            });
        }
    });

    $(document).ready(function(){
        if(document.querySelector('.gift-certificates__modal__check') !== null) {
            document.querySelector('.gift-certificates__modal__check').addEventListener('click', function(){
                setTimeout(function(){
                    var newElement = document.createElement('button');
                    newElement.className = 'btn btn__form btn__form__captcha';
                    newElement.innerText ="Оплатить";
                    document.querySelector('.gift-certificates__modal').querySelector('.captcha').replaceChild(newElement,
                        document.querySelector('.gift-certificates__modal').querySelector('.btn__form__captcha'));
                },1000);
            });
        }
    });


    if (window.matchMedia("(max-width: 767px)").matches) {
        var btn = document.querySelectorAll('.btn__container');
        var container = document.querySelectorAll('.contacts-map__container');
        for(var i = 0; i < btn.length; i++) {
            btn[i].style.display = "";
            container[i].style.display = "none";
        }
        $(btn).on('click', function(evt){
            for(var i = 0; i < btn.length; i++) {
                if (evt.target.className === btn[i].className) {
                    btn[i].style.display = "none";
                    container[i].style.display = "";
                }
            }
        });
    }

    var modalClose = document.querySelectorAll('.modal__close');
    var modal = document.querySelectorAll('.modal');

    $(window).on("click", function(evt) {
        for(var i = 0; i < modal.length; i++) {
            if (modal[i].style.display !== "none" && evt.target === modal[i]) {
                modal[i].style.display = "none";
            }
        }
    });

    $(modalClose).on("click", function(evt) {
        for(var i = 0; i < modalClose.length; i++) {
            if (modalClose[i].className === evt.target.className) {
                modal[i].style.display = "none";
            }
        }
    });

    $(document.querySelector('.get-personal-training__modal__close')).on("click", function() {
        document.querySelector('.get-personal-training__modal__close').style.display = "none";
        document.querySelector('.trainers__modal').style.display = "";
    });

    $(document.querySelector('.btn__footer')).on("click", function() {
        document.querySelector('.freeze-the-map__modal').style.display = "";
    });

    $(document.querySelector('.freeze-the-map')).on("click", function() {
        document.querySelector('.freeze-the-map__modal').style.display = "";
    });

    $(document.querySelector('.header__call')).on("click", function() {
        document.querySelector('.request-a-call__modal').style.display = "";
    });

    if(document.querySelector('.club-card__btn') !== null) {
        $(document.querySelectorAll('.club-card__btn')).on("click", function () {
            document.querySelector('.club-card__modal').style.display = "";
        });
    }

    if(document.querySelector('.success-stories___block') !== null) {
        $(document.querySelectorAll('.success-stories___block')).on("click", function () {
            document.querySelector('.success-stories__modal').style.display = "";
        });
    }

    $(document.querySelector('.trainers__modal__btn')).on("click", function() {
        document.querySelector('.get-personal-training__modal').style.display = "";
        document.querySelector('.trainers__modal').style.display = "none";
    });

    $(document.querySelector('.buy-online__btn')).on("click", function() {
        document.querySelector('.buy-online__modal').style.display = "";
    });

    $(document.querySelector('.more-about-the-map__btn')).on("click", function() {
        document.querySelector('.more-about-the-map__modal').style.display = "";
    });

    $(document.querySelector('.childrens__room__btn')).on("click", function() {
        document.querySelector('.childrens__room__modal').style.display = "";
    });

    $(document.querySelector('.tanning__studio__btn')).on("click", function() {
        document.querySelector('.tanning__studio__modal').style.display = "";
    });

    $(document.querySelectorAll('.buy-online')).on("click", function() {
        document.querySelector('.gift-certificates__modal').style.display = "";
    });


    //adaptive-menu

    if (window.matchMedia("(max-width: 1366px) and (min-width: 768px)").matches) {
        $(document.querySelectorAll('.header__menu__adaptiv__up')).on("click", function () {
            document.querySelector('.menu-adaptive').style.display = "flex";
            document.querySelector('.header__search').style.display = "block";
            document.querySelector('.selected-club').style.display = "block";
        });

        $('.menu-adaptive__close').on("click", function() {
            document.querySelector('.menu-adaptive').style.display = "none";
            document.querySelector('.header__search').style.display = "none";
            document.querySelector('.selected-club').style.display = "none";
        });

        $('.header__search').on("click", function() {
            document.querySelector('.search__form__header').style.display = "flex";
        });

        $('.search-form__close').on("click", function() {
            document.querySelector('.search__form__header').style.display = "none";
        });

        $('.submenu__1').on("click", function(evt) {
            document.querySelector('.link__1').style.display = "block";
            document.querySelector('.menu-main').classList = 'menu-main active__main';
            document.querySelector('.submenu__1').querySelector('.menu-main__link').classList = 'menu-main__link active__menu__link';
            if(document.querySelector('.link__2').style.display === "block") {
                document.querySelector('.link__2').style.display = "none";
                document.querySelector('.submenu__2').querySelector('.menu-main__link').classList = 'menu-main__link';
            }
            evt.preventDefault();

        });

        $('.submenu__2').on("click", function(evt) {
            document.querySelector('.link__2').style.display = "block";
            document.querySelector('.menu-main').classList = 'menu-main active__main';
            document.querySelector('.submenu__2').querySelector('.menu-main__link').classList = 'menu-main__link active__menu__link';
            if(document.querySelector('.link__1').style.display === "block") {
                document.querySelector('.link__1').style.display = "none";
                document.querySelector('.submenu__1').querySelector('.menu-main__link').classList = 'menu-main__link';
            }
            evt.preventDefault();
        });
    }

    if (window.matchMedia("(max-width: 767px)").matches) {
        $(document.querySelectorAll('.header__menu__adaptiv__up')).on("click", function () {
            document.querySelector('.menu-adaptive').style.display = "flex";
            document.querySelector('.header__search').style.display = "block";
            document.querySelector('.selected-club').style.display = "block";
        });

        $('.menu-adaptive__close').on("click", function() {
            document.querySelector('.menu-adaptive').style.display = "none";
            document.querySelector('.header__search').style.display = "none";
            document.querySelector('.selected-club').style.display = "none";
        });

        $('.header__search').on("click", function() {
            document.querySelector('.search__form__header').style.display = "flex";
        });

        $('.search-form__close').on("click", function() {
            document.querySelector('.search__form__header').style.display = "none";
        });

        $('.submenu__1').on("click", function(evt) {
            document.querySelector('.link__1').style.display = "block";
            document.querySelector('.menu-main').style.display = "none";
            evt.preventDefault();
        });

        $('.submenu__2').on("click", function(evt) {
            document.querySelector('.link__2').style.display = "block";
            document.querySelector('.menu-main').style.display = "none";
            evt.preventDefault();
        });

        $('.back__1').on("click", function() {
            document.querySelector('.link__1').style.display = "none";
            document.querySelector('.menu-main').style.display = "flex";
        });

        $('.back__2').on("click", function() {
            document.querySelector('.link__2').style.display = "none";
            document.querySelector('.menu-main').style.display = "flex";
        });
    }

}());