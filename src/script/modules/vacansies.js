(function() {
    var vacansiesConditions = document.querySelector('.vacansies__conditions');
    var vacansiesAll = document.querySelector('.vacansies__all');
    var jobTitle = document.querySelectorAll('.vacansies__job-title');
    var jobBlock = document.querySelectorAll('.vacansies__job-block');
    var btn = document.querySelectorAll('.vacansies__btn');
    var vacansiesRespond = document.querySelectorAll('.vacansies__respond');

    $(jobTitle).on("click", function(){
        vacansiesConditions.style.display = "none";
        ths = this;
        for (var i = 0; i < jobTitle.length; i++) {
            if(this.textContent === jobTitle[i].textContent) {
                if(jobBlock[i].style.display + '' === "none"){
                    jobBlock[i].style.display = "";
                } else {
                    jobBlock[i].style.display = "none";
                }
            }
        }
    });

    $(btn).on("click", function(){
        ths = this;
        var active = document.querySelector('.active');
        for (var i = 0; i < btn.length; i++) {
            if(this.value === btn[i].value) {
                if (active !== null) {
                    active.classList = "vacansies__respond";
                    active.style.display = "none";
                }
                vacansiesRespond[i].style.display = "";
                vacansiesRespond[i].classList = "vacansies__respond active";
            }
        }
    });

    if (window.matchMedia("(max-width: 768px)").matches) {
        $(btn).on("click", function(){
            vacansiesAll.style.display = "none";
        });
    }

    function createEl(captcha, inp){
        var newElement = document.createElement('button');
        newElement.className = 'btn btn__form btn__form__captcha';
        newElement.innerText ="Отправить";
        captcha.replaceChild(newElement, inp);
    }


    $(document).ready(function(){
        if(document.querySelector('.vacansies__check') !== null) {
            var btn = document.querySelectorAll('.vacansies__check');
            $(btn).on('click', function(evt){
                var btn = document.querySelectorAll('.vacansies__check');
                var inp = document.querySelectorAll('.btn__form__captcha__vacansies');
                var captcha = document.querySelectorAll('.captcha__vacansies');

                for (var i = 0; i < btn.length; i++){
                    if(evt.target.className === btn[i].className) {
                        setTimeout(createEl(captcha[i], inp[i]),1000);
                    }
                }

            });
        }
    });
})();