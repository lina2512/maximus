var key = 'ruso_q_712899_example';
const body = document.querySelector('body');

body.classList = window.localStorage[key];

window.addEventListener('storage', function(event) {
    if (event.key !== key) {
        return;  // Если прислали не наши данные, ничего не делаем
    }
    body.classList = window.localStorage[key];
});