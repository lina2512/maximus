'use strict';

(function () {
    var btnSliders = document.querySelectorAll(".btn__sliders");
    var slide = document.querySelectorAll(".slide");

    $(btnSliders).on("click", function(evt) {
        var slideActive = document.querySelector(".active__slides");
        var btnActive = document.querySelector(".active__slide");
        for(var i = 0; i < slide.length; i++) {
            if(btnSliders[i].className === evt.target.className && btnActive.className !== evt.target.className) {
                slide[i].style.display = "";
                slideActive.style.display = "none";
                slide[i].classList.add("active__slides");
                slideActive.classList.remove("active__slides");
                btnSliders[i].classList.add("active__slide");
                btnActive.classList.remove("active__slide");
            }
        }
    })
})();